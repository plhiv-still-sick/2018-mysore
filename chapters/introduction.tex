\chapter{Introduction} % start of your main text

\setlength\epigraphwidth{12cm}
\setlength\epigraphrule{0pt}
\epigraphfontsize{\small\itshape}
\epigraph{"My read of the tea leaves was that we had turned a corner in AIDS: we might now convert it from a fatal disease to a chronic disease, a chronic disease to which the patient would eventually succumb but in the interim might live a useful and productive life."}{--- \textup{Abraham Varghese}, My Own Country}

Effective antiretroviral therapy (ART) has led to dramatic decline in mortality and morbidity among People Living with HIV/AIDS (PLHA) in India and globally \autocite{palellaDecliningMorbidityMortality1998,mocroftDeclineAIDSDeath2003,teixeiraAntiretroviralTreatmentResourcepoor2004,marinsDramaticImprovementSurvival2003,nacoAnnualReportNational}. The number of new infections in India is estimated to be 80 thousand at present.\autocite{hiv/aidsUNAIDSDATA20172017} Presently there are an estimated 2.1 million PLHA in India.\autocite{hiv/aidsUNAIDSDATA20172017} About 62,000 AIDS-related deaths occured in 2016\autocite{hiv/aidsUNAIDSDATA20172017}. It is reasonable to expect that a proportionate number of PLHA are hospitalized every year due to AIDS-related illnesses.

Similar to the global trend, the prevalence, incidence, and deaths related to HIV/AIDS are declining in India.\autocite{FactSheetLatest} Prevalence among adults in India was estimated to be 0.38\% in 2001-03 (at the peak) and dropped to an estimated 0.26\% in 2015.\autocite{nacoAnnualReportNational} Incidence has declined by 66\% between 2000 and 2015\autocite{NACOANNUALREPORT}. Between 2007 and 2015 AIDS Related Deaths (ARD) decreased by 54\%.\autocite{nacoAnnualReportNational}

The number of people on ART has correspondingly increased to the present level of 49\%.\autocite{hiv/aidsUNAIDSDATA20172017} These trends make HIV a chronic disease in many patients bringing with it morbidities due to prolonged ARV exposure, inflammation, and immunodeficiency.\autocite{deeksEndAIDSHIV2013}

This study brings out the patients’ perspective on how they are dealing with this chronic illness and how the systems of care are able to cater to their needs.

Of all the PLHA in India, only 77\% have been diagnosed to be infected with HIV. And only 49\% (1 million) are started on ART\autocite{hiv/aidsUNAIDSDATA20172017}. Viral load testing is not available to majority of Indians. Applying global rate (82\%) of viral load suppression leaves 60\% of PLHA in India not virologically suppressed. 

\begin{table}
    \begin{tabularx}{\textwidth}{ll}
        \toprule
        Total number of PLHA in India & 2.1 million (100\%)\\
        Proportion who are diagnosed  & 77\%\\
        Proportion who are on ART     & 49\%\\
        Proportion who are 
        virologically suppressed      & 40\%?\\
        \bottomrule
    \end{tabularx}
    \caption{Where is India in the 90-90-90 cascade?\label{tab:cascade}}
\end{table}

India recently switched to test and treat policy in 2017\autocite{HealthMinistryLaunches} in alignment with WHO\autocite{WHOConsolidatedGuidelines} as it is more beneficial to start ART as early as possible.\autocite{groupTrialEarlyAntiretrovirals2015,groupInitiationAntiretroviralTherapy2015} After 12 months of ART virologic suppression is achieved in majority of PLHA.\autocite{mcmahonViralSuppression122013} With effective ART, the incidence of opportunistic infections declines.\autocite{lowIncidenceOpportunisticInfections2016}


National AIDS Control Organization (NACO) has been undertaking awareness raising programs since the first phase of the National AIDS Control Program (NACP) in 1992.\autocite{AwarenessRaisingNational,NACPNationalAIDS} Integrated Counseling and Testing Centres have been set up throughout the country where free HIV testing is provided.\autocite{IntegratedCounsellingTesting} AIDS Act 2017 gives persons affected by HIV various rights.\autocite{HUMANIMMUNODEFICIENCYVIRUS} Yet as described, more than a million PLHA are not yet diagnosed to be infected in India. Often people newly diagnosed to be HIV positive have very low CD4 counts and high level of immunosuppression which indicate that they were infected years prior to diagnosis.\autocite{bishnuAssessmentClinicoimmunologicalProfile2014} Poor awareness, stigma, and related reluctance to be tested has been implicated in late diagnosis.\autocite{mahajanStigmaHIVAIDS2008} With high viral loads at the beginning of ART, drug resistance develops easily and in turn leads to failure.\autocite{harriganPredictorsHIVDrugresistance2005}


According to older NACO guidelines, PLHA who had good CD4 counts were not started on ART compulsorily. Retention in HIV care between testing and treating has been shown to be difficult.\autocite{rosenRetentionHIVCare2011} Many such people who were on pre-ART care have died, transferred out, or defaulted from care.\autocite{chakravartyWhyPatientsPreAnti2016,shastriJourneyAntiretroviralTherapy2013} Inconvenient clinic timing, needing multiple modes of transport, perceived improvement in health, high distance of the ART centre from home, lack of social support, and financial difficulty have been reported as reasons for defaulting from care.\autocite{chakravartyWhyPatientsPreAnti2016}

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{assets/factors-affecting-hospitalization.pdf}
    \caption{Factors leading to hospitalization \label{img:factors}}
\end{figure}

Suboptimal adherence contributes to virologic failure.\autocite{VirologicFailureDefinition} Poor adherence has been attributed to patient factors (drug use, alcohol use, depression, lack of family care, social stigma), medication regimen (pill burden, side effects), patient-doctor relationship, and system of care (cost of medication, lack of access).\autocite{chesneyFactorsAffectingAdherence2000,sarnaAdherenceAntiretroviralTherapy2008} A systematic review by Mhaskar et al. in 2013 finds cost of medication, lack of access to medication, and adverse events as the most common reasons to poor adherence in the studies included.\autocite{mhaskarAdherenceAntiretroviralTherapy2013} Drug resistance and drug toxicity are the other factors that can contribute to virologic failure.\autocite{VirologicFailureDefinition}

Many studies have been conducted on the reasons for suboptimal adherence. There are few such qualitative studies from India. The results of these are mentioned in chapter \ref{chap:literature}. The proximal reasons of hospitalization (AIDS related illnesses and bacterial infections)\autocite{alvarezbarrenecheHospitalizationCausesOutcomes2017,fordCausesHospitalAdmission2015} and clinical profile of hospitalized (low CD4, anemia)\autocite{chakravartyWhyPatientsPreAnti2016} has been studied in various geographic regions.

While the proximal and distal causes of hospitalization are known, there is no narrative on how distal reasons lead to proximal reasons from the patient perspective.\autocite{WHOChapter} Also there is few such studies from rural areas. This study aims to fill this gap thereby leading to a better understanding of the non-pathologic progression of this illness.

